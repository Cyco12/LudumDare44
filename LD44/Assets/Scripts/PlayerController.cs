﻿//Libraries/APIs
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using TMPro;


//Player Controller class and code
public class PlayerController : MonoBehaviour
{
    // Variables
    Rigidbody2D rb;
    Vector3 movement;

    public float speed;



    // Game boundary class
    /*[System.Serializable]
    public class Boundary
    {
        public float xMin, xMax, yMin, yMax;
    }*/


    //public Boundary boundary; // Player Boundary

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {

        movement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f);
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(movement.x * speed, movement.y * speed);
    }
}